﻿using ChoETL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Exercise2
{
    class Program
    {
        private static List<string> firstNames = new List<string>();
        private static List<string> lastNames = new List<string>();
        private static List<string> addresses = new List<string>();
        private static List<string> phoneNumbers = new List<string>();


        private static string csvPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"Data.csv");
        private static string csvOutputpath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"OrderedNames.txt");
        private static string addressesOutputpath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"OrderedAddresses.txt");

        static void Main(string[] args)
        {
            Console.WriteLine("***** Checking and deleting any existing files *****");
            DeleteExistingFile();

            Console.WriteLine("***** Loading and processing CSV file *****");
            LoadAndProcessCsv();

            SortByNamesAndFrequency();
            Console.WriteLine($"***** Arranged first names and last names file written to {csvOutputpath} *****");

            ArrangeAddress();
            Console.WriteLine($"***** Arranged addresses file written to {addressesOutputpath} *****");

            Console.WriteLine("Press <ENTER> to continue");

            Console.ReadLine();

        }

        private static void DeleteExistingFile()
        {
            if (File.Exists(csvOutputpath))
            {
                File.Delete(csvOutputpath);
            }

            if (File.Exists(addressesOutputpath))
            {
                File.Delete(addressesOutputpath);
            }
        }

        private static void LoadAndProcessCsv()
        {
            using (var p = new ChoCSVReader<Person>(csvPath).WithFirstLineHeader(true))
            {
                foreach (var rec in p)
                {
                    firstNames.Add(rec.FirstName);
                    lastNames.Add(rec.LastName);
                    addresses.Add(rec.Address);
                    phoneNumbers.Add(rec.PhoneNumber);
                }
            }
        }

        private static void SortByNamesAndFrequency()
        {
            var namesFrequency     = firstNames.GroupBy(x => x).ToDictionary(x => x.Key, x => x.Count());
            var lastNamesFrequency = lastNames.GroupBy(x => x).ToDictionary(x => x.Key, x => x.Count());

            var firstNameLastNameFrequency = namesFrequency.Union(lastNamesFrequency).ToDictionary(pair => pair.Key, pair => pair.Value);

            foreach (KeyValuePair<string, int> pair in firstNameLastNameFrequency.OrderByDescending(x => x.Value).ThenBy(x => x.Key))
            {
                File.AppendAllText(csvOutputpath, $"{pair.Key}, {pair.Value} {Environment.NewLine}");
            }
        }

        private static void ArrangeAddress()
        {
            var addresse = Addresses();

            foreach (Address address1 in addresse.OrderBy(x => x.Street))
            {
                File.AppendAllText(addressesOutputpath, $"{address1.FullAddress}{Environment.NewLine}");
            }
        }

        private static List<Address> Addresses()
        {
            List<Address> addresse = addresses.Select(s => new
                {
                    FullAddress = s.Trim(),
                    Tokens = s.Trim().Split()
                })
                .Where(x => x.Tokens.Length > 1 && x.Tokens[0].All(Char.IsDigit))
                .Select(x => new Address
                {
                    FullAddress = x.FullAddress,
                    Street = String.Join(" ", x.Tokens.Skip(1)),
                    Number = int.Parse(x.Tokens[0])
                })
                .OrderBy(addr => addr.Number)
                .ToList();
            return addresse;
        }
    }
}
